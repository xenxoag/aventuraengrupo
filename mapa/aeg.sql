create database AVENTURA;
create schema aeg_schema;

create table Ofertas (
id serial not null,
deporte varchar(50) not null
nombre_oferta varchar(50) not null,
lugar varchar(100) not null,
decripcion varchar(100) l,
precio money not null,
provincia varchar(100) not null,
fecha date not null,
imagen varchar (256);
);


create table Usuario (
id serial not null,
nombre_usuario varchar(50) not null,
apellidos varchar(50) not null,
email varchar(50) not null,
"password" varchar (256)
);

create table Provedores (
id serial not null,
nombre_prov varchar(50) not null,
nif varchar(50) not null,
direccion varchar(50) not null,
email varchar(50) not null,
nombre_cont varchar(50) not null,
telefono numeric not null,
"password"  varchar (256)

);