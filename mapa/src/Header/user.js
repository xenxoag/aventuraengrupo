import Login from "./Login"
import React from 'react'
import { useSelector } from 'react-redux'
import PrivateUser from './PrivateUser'

const User = () => {

    const user = useSelector(s => s.user)
    return (
      <div className="App">
        {!user && <Login />}
        {user && <PrivateUser />}
      </div>
    )
  }
  export default User