import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import { Link } from 'react-router-dom'
import './Registro.css'

const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const Registro = () => {
  const [nombre_usuario, setNombre_usuario] = useFormField()
  const [apellidos, setApellidos] = useFormField()
  const [email, setEmail] = useFormField()
  const [password, setPassword] = useFormField()

  const history = useHistory()
  const [mensaje, setMensaje] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    const user = { nombre_usuario, apellidos, email, password }
    setMensaje(false)
    try {
      const ret = await fetch('http://localhost:8080/users', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json'
          // 'Authorization': localStorage.getItem('token') // Esto en todas las llamadas autenticadas
        }
      })
      const data = await ret.json()
      // localStorage.setItem('token', data.token) // Esto solo en login, para guardar el token
      history.push(`/users/${data.id}`)
      if (data.success) {
        // ok! todo bien
      } else {
        setMensaje(true)
      }
    } catch (err) {
      console.warn('Error:', err)
      console.log('entra')
      setMensaje(true)
    }
  }
  return (
    <div class="regBack">
      <div id="formContent" class="fadeInDown">
      <p className="usinfo">Usuario</p>
        <h1 className="inactive underlineHover" id="entrare"><Link to="/User">ENTRAR</Link></h1>
        <h1 class="active" id="registrare">Registrarse</h1>
        <p className="adinfo" class="fadeIn null">Cubre los campos solicitados</p>
        <form onSubmit={handleSubmit}>
          <label>
            {/* Nombre: */}
   <input name='nombre_usuario' required value={nombre_usuario} onChange={setNombre_usuario}
              type="text" id="nombreempresa" class="fadeIn first" placeholder="Nombre de usuario" />
          </label>
          <label>
            {/* Apellidos: */}
   <input name='apellidos' required value={apellidos} onChange={setApellidos} 
   type="text" id="nif" class="fadeIn second" placeholder="apellidos"/>
          </label>
          <label>
            {/* Email: */}
   <input name='email' required value={email} onChange={setEmail} 
               type="text" id="direccion" class="fadeIn third" placeholder="email"/>
          </label>
          <label>
            {/* Contraseña: */}
   <input name='password' required value={password} onChange={setPassword} 
    type="text" id="password" class="fadeIn fourth" placeholder="Password"/>
          </label>
          <input type="submit" class="fadeIn fiveth" value="ENTRA" />
          {mensaje && <div>Error, usuario registrado</div>}
        </form>
      </div>
      </div>
      )
    
    }
export default Registro