import React, { useState } from 'react'
import { useDispatch} from 'react-redux'
import './Login.css'
import { Link, useHistory  } from 'react-router-dom'
const jwt = require('jsonwebtoken')





const Login = () => {
  const dispatch = useDispatch()
  const login = (user) => {
    dispatch({ type: 'login', user })
    dispatch({ type: 'logoutProov'})
  }
  const [nombre_usuario, setNombre_usuario] = useState('')
  const [password, setPassword] = useState('')
  const [mensaje, setMensaje] = useState(false)
  const historiti = useHistory()

  const handleSubmit = async (e) => {
    e.preventDefault()
    const user = { nombre_usuario, password }
    setMensaje(false)
    try {
      const ret = await fetch('http://localhost:8080/login', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const data = await ret.json()

      if (data.error === true) {
        setMensaje(true)
        return
      }
      if (!data.success === true || !('token' in data)) {
        throw new Error("error inesperado: Bug en la aplicación?")
      }

      let decoded = jwt.decode(data.token)
      decoded.token = data.token


      localStorage.setItem("token", data.token)
      localStorage.setItem("usuario", decoded.nombre_usuario)
      login({
        username: decoded.nombre_usuario,
        token: data.token
      })
      historiti.push(`/`)


      setMensaje(false)
      console.log(data)

     
    } catch (err) {
      console.log(err)
    }

  }


  return (
    <div className="logback">
      <div id="lablanca" className="fadeInDown">
      <p className="usinfo">Usuario</p>
        <h1 class="active" id="entr">ENTRAR </h1>
        <h2 class="inactive underlineHover" id="entrare"><Link to="/Registro">Registrarse</Link></h2>
        <p className="adinfo" class="fadeIn null">Compreta para acceder a tu perfil</p>
        <form  onSubmit={handleSubmit}>
          {mensaje && <p>usuario no encontrado</p>}
 
        <input
              name="nombre_usuario"
              required
              value={nombre_usuario}
              onChange={e => setNombre_usuario(e.target.value)}
              type="text" id="login" class="fadeIn second"  placeholder="Usuario"
            />
        <input
              name="password"
              required
              value={password}
              onChange={e => setPassword(e.target.value)}
              type="text" id="password" class="fadeIn third"  placeholder="Password"
            />
        <input type="submit" class="fadeIn fourth" value="ENTRA" /> 
        
        </form>
        </div>
    </div>
        )
      }
      
export default Login