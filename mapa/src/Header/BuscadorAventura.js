import React, {useState} from 'react';
import {  useHistory } from 'react-router-dom'
import './BuscadorAventura.css'

const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}

const SearchField = () => {
  const [deporte, setDeporte] = useFormField()
  const [fecha, setFecha] = useFormField()
  const [provincia, setProvincia] = useFormField()
  const [precio, setPrecio] = useFormField()
  const history = useHistory()
  const handleSubmit = async (e) => {
    e.preventDefault()
    let query = []
    if (deporte) {
      query.push('deporte='  +deporte)
    }
    if (fecha) {
      query.push('fecha=' + fecha)
    }
    //history.push(`/ofers/${deporte}/${fecha}/${provincia}/${precio}`)  
    // localhost:3000/ofers?deporte=escalada&fecha=30-01-2020
    history.push(`/ofers/busqueda?${query.join('&')}`)    
  }

  return (

    <form id="container" onSubmit={handleSubmit}>
        
          <div>

            <label className="TextContainerRow"> ACTIVIDAD</label> 
            <div >
            <select 
            name="deporte"
            className="containerRow" 
            value={deporte} 
            onChange={setDeporte}>
              <option  selected></option>
              <option>Puenting</option>
              <option>RAFTING</option>
              <option>erg</option>
              <option>escalada</option>
            </select>
         </div>
        </div>

        <div>
                  <label className="TextContainerRow"> PROVINCIA</label> 
            <div>
            <select 
            className="containerRow"
            name="provincia"
            value={provincia} 
            onChange={setProvincia}>
              <option className="TextContainerRow" selected></option>
              <option>Coruña</option>
              <option>Lugo</option>
              <option>Ourense</option>
              <option>Pontevedra</option>
              
           </select>
           </div>
        </div>

        <div>
              <option className="TextContainerRow" selected>FECHA</option>
              <label>
              <input className="containerRowFecha" type="date"
                name="fecha"
                id="containerRow" 
                value={fecha} 
                onChange={setFecha}/>
              </label> 
        </div>

        <div>
              <option className="TextContainerRow" selected>PRECIO MAXIMO</option>
              <label>
              <input 
              id="containerRowPrecio" 
              type="number"  min="0" max="10000" step="5" 
              name="precio"
                className="containerRow" 
                value={precio} 
                onChange={setPrecio}></input>
              </label> 
        </div>


        <div className="botonSearch">
          <button type="submit" className="textBotonSearch">SEARCH</button>
        </div>
      
    </form>
  )
}

export default SearchField 