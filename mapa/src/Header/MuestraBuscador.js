import React, { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'
import '../Ofertas/oferta.css'
import '../App.css'


  
const MuestraBuscador =()=>{
    const  location = useLocation()
  const [ oferta, setOferta ] = useState()
    console.log(location)
  useEffect(() => {
        fetch('http://localhost:8080/ofers/' )
            .then(r => r.json())
            .then(data => setOferta(data))
    }, [])
    
    /*if (!oferta) return 'loading...'
    const entries = oferta.filter(i => 
        i.deporte === deporte 
        && i.provincia === provincia
        && i.fecha && i.fecha.substring(0,10) === fecha
        && i.precio  <= precio)
    if (!entries.length) return 'no existe ninguna oferta de ese deporte'*/
const entries = []
  return(
 <div className="ofertas">
            {entries.map(oferta =>
                <Link to={`/oferta/${oferta.id}`}>
                    <div className="carta-oferta" key={oferta.deporte}>
                        <header>
                            <h1>{oferta.deporte}</h1>
                        </header>
                        <section className="ofer">
                        <img src={oferta.imagen }width="200" height="150"/>
                            <ul>
                                <li>
                                    <h2>{oferta.nombre_oferta}</h2>
                                </li>
                                <li>
                                    <h3>{oferta.lugar}</h3>
                                </li>
                            </ul>
                        </section>
                        <footer >
                            <p>{oferta.precio}</p>
                           
                        </footer>
                    </div>
                </Link>
            )}
        </div>
    )
}



 export default MuestraBuscador
