import React from 'react'
import Portada from './Portada/Portada'
import Header from './Header/Header'
import './App.css'
import Ofer from './Ofertas/ofer'

import Footer from './Footer/Footer';
import Registro from './Header/Registro';
import Nosotros from './Footer/Nosotros';
import MiniOfertas from './Ofertas/MiniOfertas';
import Promotor from './Footer/Promotor'
import MuestraDeportes from './Opciones/MuestraDeportes'
import MuestraLugar from './Opciones/MuestraLugar'
import MuestraFecha from './Opciones/MuestraFecha'
import MuestraPrecio from './Opciones/MuestraPrecio'
import User from './Header/user'
import Entrar from './Header/entrar'
import CompraRealizada from './Ofertas/CompraRealizada'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './Footer/Reducers'
import { save, load } from 'redux-localstorage-simple'
import EmpreRegister from './Footer/EmpreRegister'
import CrearOferta from './Footer/CrearOferta'
import PromotorOfertas from './Footer/PromotorOfertas'
import DeleteOffer from './Footer/DeleteOffer'
import EditOffer from './Footer/EditOfert'



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  rootReducer, load(),
  composeEnhancers(applyMiddleware(save())))

function App() {

  return (
  
      <Provider store={store}>
        <Router>
          <div >
            <Header />                      
            <Switch>
              <Route path='/' exact>
                <div>
                  <Portada />     
                  <MiniOfertas />
                </div>
              </Route>
              <Route path='/Entrar' exact>
                <Entrar />
              </Route>
              <Route path='/user' exact>
                <User />
              </Route>
              <Route path='/registro' exact>
                <Registro />
              </Route>
              <Route path='/EmpreRegister' exact>
                <EmpreRegister />
              </Route>
              <Route path='/CrearOferta' exact>
                <CrearOferta />
              </Route>
              <Route path="/ofers/:id" exact>
                <Ofer />
              </Route>
              <Route path="/CompraRealizada" exact>
                <CompraRealizada />
              </Route>
              <Route path='/promotor' exact>
                <Promotor />
              </Route>
              <Route path='/nosotros' exact>
                <Nosotros />
              </Route>
              <Route path='/deporte/:deporte' exact>
                <MuestraDeportes />
              </Route>
              <Route path='/provincia/:provincia' exact>
                <MuestraLugar />
              </Route>
              <Route path='/fecha/:fecha' exact>
                <MuestraFecha />
              </Route>
              <Route path='/precio/:precio' exact>
                <MuestraPrecio />
              </Route>
              <Route path='/promotor/:name' exact>
                <PromotorOfertas />
              </Route>
              <Route path='/promotor/delete/:id' exact>
                <DeleteOffer />
              </Route>
              <Route path='/promotor/modificar/:id' exact>
                <EditOffer />
              </Route>
            </Switch>
            <Footer />
          </div>
        </Router>
      </Provider>
    

  );
}

export default App;
