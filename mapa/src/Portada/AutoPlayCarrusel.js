import React from 'react';
import Slider from 'infinite-react-carousel';

import PuentingImg from './Imagenes_AutoPlayCarrusel_GIF/PUENTING.gif';
import MotoAguaImg from './Imagenes_AutoPlayCarrusel_GIF/MOTO DE AGUA.gif';
import BarranquismoImg from './Imagenes_AutoPlayCarrusel_GIF/BARRANQUISMO.gif';
import SurfingImg from './Imagenes_AutoPlayCarrusel_GIF/SURF.gif';
import Rutas_4x4Img from './Imagenes_AutoPlayCarrusel_GIF/TODOTERRENO.gif';
import MBTImg from './Imagenes_AutoPlayCarrusel_GIF/DESCENSO_MBT.gif';
import ParapenteImg from './Imagenes_AutoPlayCarrusel_GIF/PARAPENTE.gif';
import QuadImg from './Imagenes_AutoPlayCarrusel_GIF/QUAD.gif';
import RaftingImg from './Imagenes_AutoPlayCarrusel_GIF/RAFTING.gif';
import Rutas_CaballoImg from './Imagenes_AutoPlayCarrusel_GIF/CABALLO.gif';
import Rutas_KayakImg from './Imagenes_AutoPlayCarrusel_GIF/KAYAK.gif';
import Rutas_MotoImg from './Imagenes_AutoPlayCarrusel_GIF/MOTO.gif';
import SubmarinismoImg from './Imagenes_AutoPlayCarrusel_GIF/BUCEO.gif';
import WindsurfImg from './Imagenes_AutoPlayCarrusel_GIF/WINDSURF.gif';

import './AutoPlayCarrusel.css'


const CarouselAutoPlay = () => {

    const settings = {
  
        arrowsScroll: 1,
        arrowsBlock: false,
        autoplay: true,
        autoplayScroll: 1,
        autoplaySpeed: 6000,
        duration: 50,
        slidesToShow: 2,
    };
    

    return (
        <div className='AutoPlayCarrusel' >
            <span></span>
             <Slider {...settings}>

                <div >
                    <div className='styleImagen'  style={{ backgroundImage: `url(${SubmarinismoImg})` }}>
                    <p className= 'textoImagenes'>Descubre los nuevos paraisos del buceo</p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${SurfingImg})` }}>
                    <p className= 'textoImagenes'>Subete a las olas mas espectaculares </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_4x4Img})` }}>
                    <p className= 'textoImagenes'>Conoce a otros viajeros   </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${MBTImg})` }}>
                    <p className= 'textoImagenes'>Únete a un grupo con tus mismas aficiones   </p>  
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${ParapenteImg})` }}>
                    <p className= 'textoImagenes'>Tenemos las actividades mas exclusivas </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${QuadImg})` }}>  
                     <p className= 'textoImagenes'>Crea tu propio grupo de viajeros   </p> 
                    </div>
                </div>



                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_CaballoImg})` }}>
                    <p className= 'textoImagenes'>Unete a un grupo y disfruta de la naturaleza </p> 
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_KayakImg})` }}>
                    <p className= 'textoImagenes'>Existen grupos con tus mismas aficiones </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${Rutas_MotoImg})` }}>
                    <p className= 'textoImagenes'>Encuentra viajeros con tu misma ruta  </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagen'  style={{ backgroundImage: `url(${WindsurfImg})` }}>
                    <p className= 'textoImagenes'>Unete a nuestra comunidad y descubre nuevas actividades </p>
                    </div>
                </div>

            </Slider>
        </div>
    );

}
export default CarouselAutoPlay