import React from 'react';
import Slider from 'infinite-react-carousel';

import MotoAguaImg from './Imagenes_AutoPlayCarrusel_GIF/MotosDeAgua.gif';
import PuentingImg from './Imagenes_AutoPlayCarrusel_GIF/PUENTING.gif';
import BarranquismoImg from './Imagenes_AutoPlayCarrusel_GIF/BARRANQUISMO.gif';
import SurfingImg from './Imagenes_AutoPlayCarrusel_GIF/SURF.gif';
import Rutas_4x4Img from './Imagenes_AutoPlayCarrusel_GIF/TODOTERRENO.gif';
import MBTImg from './Imagenes_AutoPlayCarrusel_GIF/DESCENSO_MBT.gif';
import ParapenteImg from './Imagenes_AutoPlayCarrusel_GIF/PARAPENTE.gif';
import QuadImg from './Imagenes_AutoPlayCarrusel_GIF/QUAD.gif';
import RaftingImg from './Imagenes_AutoPlayCarrusel_GIF/RAFTING.gif';
import Rutas_CaballoImg from './Imagenes_AutoPlayCarrusel_GIF/CABALLO.gif';
import Rutas_KayakImg from './Imagenes_AutoPlayCarrusel_GIF/KAYAK.gif';
import Rutas_MotoImg from './Imagenes_AutoPlayCarrusel_GIF/MOTO.gif';
import SubmarinismoImg from './Imagenes_AutoPlayCarrusel_GIF/BUCEO.gif';
import WindsurfImg from './Imagenes_AutoPlayCarrusel_GIF/WINDSURF.gif';

import './AutoPlayCarruselGIF.css'

//import './AutoPlayCarrusel.css'

const CarouselAutoPlayGIF = () => {

    const settings = {

        arrowsScroll: 1,
        arrowsBlock: false,
        autoplay: true,
        autoplayScroll: 1,
        autoplaySpeed: 10000,
        duration: 50,
        slidesToShow: 2,
    };


    return (
        <div className='AutoPlayCarruselGIF' >
            <span></span>
            <Slider {...settings}>
                <div >
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${MotoAguaImg})` }}>
                        <p className='textoImagenesGIF'>Disfruta de la velocidad sobre el Mar</p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${PuentingImg})` }}>
                        <p className='textoImagenesGIF'>Da el salto a una nueva Aventura </p>
                    </div>
                </div>

                <div >
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${SubmarinismoImg})` }}>
                        <p className='textoImagenesGIF'>Descubre los nuevos paraisos del buceo</p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${SurfingImg})` }}>
                        <p className='textoImagenesGIF'>Subete a las olas mas espectaculares </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${Rutas_4x4Img})` }}>
                        <p className='textoImagenesGIF'>La aventura de conocer otros caminos   </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${MBTImg})` }}>
                        <p className='textoImagenesGIF'>Crea un grupo con tus mismas aficiones   </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${ParapenteImg})` }}>
                        <p className='textoImagenesGIF'>Tenemos las actividades mas exclusivas </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${QuadImg})` }}>
                        <p className='textoImagenesGIF'>Crea tu propio grupo de Viajeros   </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${RaftingImg})` }}>
                        <p className='textoImagenesGIF'>Disfruta de la Naturaleza  </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${Rutas_CaballoImg})` }}>
                        <p className='textoImagenesGIF'> Respetamos el Medio Ambiente </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${Rutas_KayakImg})` }}>
                        <p className='textoImagenesGIF'>Existen grupos con tus mismas aficiones </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${Rutas_MotoImg})` }}>
                        <p className='textoImagenesGIF'>Busca compañeros para tu próxima Ruta  </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${WindsurfImg})` }}>
                        <p className='textoImagenesGIF'>Unete a nuestra Comunidad </p>
                    </div>
                </div>

                <div>
                    <div className='styleImagenGIF' style={{ backgroundImage: `url(${BarranquismoImg})` }}>
                        <p className='textoImagenesGIF'>Descubre nuevas Actividades </p>
                    </div>
                </div>

            </Slider>
        </div>
    );

}

export default CarouselAutoPlayGIF