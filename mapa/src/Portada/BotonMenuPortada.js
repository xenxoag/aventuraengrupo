
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { Link } from 'react-router-dom';

import './BotonMenuPortada.css'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  
  },
  paper: {
    marginRight: theme.spacing(2),
  },
}));

const MenuListComposition = ()  => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen(prevOpen => !prevOpen);
  };

  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <div className={classes.root}>
   
      <div className= 'Menu'>
        <button
        className = 'MenuButton'
         ref={anchorRef}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
        >
          <p className= 'TextMenuButton'>ACTIVIDADES</p>
         
        </button>
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList className = 'BoxMenuButton'  autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                    <MenuItem onClick={handleClose}> <Link path='https://......../vela'></Link>Vela</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rafting'></Link>Rafting</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../kayak'></Link>Kayak</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../alpinismo'></Link>Alpinismo</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_motociclismo'></Link>Rutas Motociclismo</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_quad'></Link>Rutas En Quad</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_caballo'></Link>Rutas A Caballo</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../parapente'></Link>Parapente</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../paracaidismo'></Link>Paracaidismo</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_mountain_bike'></Link>Rutas Mountain Bike</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../puenting'></Link>Puenting</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../moto_de_agua'></Link>Moto De Agua</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../submarinismo'></Link>Submarinismo</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_descenso_MTB'></Link>Rutas Descenso MTB</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../rutas_todo_terreno'></Link>Rutas Todo Terreno</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../surf'></Link>Surf</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../windsurf'></Link>Windsurf</MenuItem>
                    <MenuItem onClick={handleClose}> <Link path='https://......../barranquismo'></Link>Barranquismo</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    </div>
  );
}

export default MenuListComposition