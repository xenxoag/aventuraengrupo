import React from 'react';
import {Link} from'react-router-dom';
import Slider from 'infinite-react-carousel';

import windsurfImg from './Imagen_Carrusel/windsurf.jpeg';
import puentingImg from './Imagen_Carrusel/puenting.jpeg';
import velaImg from './Imagen_Carrusel/vela.jpeg';
import barranquismoImg from './Imagen_Carrusel/barranquismo.jpeg';
import paracaidismoImg from './Imagen_Carrusel/paracaidismo.jpeg';
import motosdeaguaImg from './Imagen_Carrusel/motosdeagua.jpeg';
import submarinismoImg from './Imagen_Carrusel/submarinismo.jpeg';
import descensoMTBImg from './Imagen_Carrusel/descensoMTB.jpeg';
import parapenteImg from './Imagen_Carrusel/parapente.jpeg';
import rutaCaballoImg from './Imagen_Carrusel/rutaCaballo.jpeg';
import rutaQuadImg from './Imagen_Carrusel/rutaQuad.jpeg';
import alpinismoImg from './Imagen_Carrusel/alpinismo.jpeg';
import rutaTodoterrenoImg from './Imagen_Carrusel/rutaTodoterreno.jpeg';
import surfImg from './Imagen_Carrusel/surf.jpeg';
import rutaMountainBikeImg from './Imagen_Carrusel/rutaMountainBike.jpeg';
import kayakImg from './Imagen_Carrusel/kayak.jpeg';
import ciclismoImg from './Imagen_Carrusel/ciclismo.jpeg';
import './Carrusel.css'

const Carrusel = () => {

  const settings = {
    
    autoplay: true,
    autoplaySpeed: 5000,
    arrowsScroll: 2,
    arrowsBlock: true,
    autoplayScroll: 1,
    centerPadding: 10,
    overScan: 6,
    slidesToShow: 10,
    wheelScroll: 1
  };
  return (
    <div className='Carrusel'>

      <Slider {...settings}>

      <nav>
        <Link to='./deporte/escalada'>

            <div title='BARRANQUISMO' >
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${barranquismoImg})` }}>
                <p className='imagenCarruselText'>ESCALADA</p>
              </div>
            </div>
            </Link>
        </nav>
        <nav>
          <Link to='./deporte/VELA'>
            <div title='REGATAS A VELA'>
            <div className='imagenCarrusel' style={{ backgroundImage: `url(${velaImg})` }}>
                <p className='imagenCarruselText'>VELA</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/MOUNTAINBIKE'>
            <div  title='RUTAS MOUNTAINBIKe'>
              <div className='imagenCarrusel'style={{ backgroundImage: `url(${rutaMountainBikeImg})` }}>
                <p className='imagenCarruselText'>MOUNTAINBIKE</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/CICLOTURISMO'>
            <div title='RUTAS DE CICLOTURISMO'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${ciclismoImg})` }}>
                <p className='imagenCarruselText'>CICLISMO</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/KAYAK'>
            <div title='RUTAS EN KAYAK'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${kayakImg})` }}>
                <p className='imagenCarruselText'>KAYAK</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/ALPINISMO'>

            <div title='RUTAS DE ALPINISMO'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${alpinismoImg})` }}>
                <p className='imagenCarruselText'>ALPINISMO</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/puenting'>

            <div title='PUENTING'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${puentingImg})` }}>
                <p className='imagenCarruselText'>PUENTING</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/QUAD'>

            <div title='RUTAS EN QUAD' >
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${rutaQuadImg})` }}>
                <p className='imagenCarruselText'>QUAD</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/RUTAS A CABALLO'>
            <div title='RUTAS A CABALLO' >
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${rutaCaballoImg})` }}>
                <p className='imagenCarruselText'>EQUITACION</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/PARAPENTE'>

            <div title='PARAPENTE'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${parapenteImg})` }}>
                <p className='imagenCarruselText'>PARAPENTE</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/PARACAIDISMO'>

            <div title='PARACAIDISMO'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${paracaidismoImg})` }}>
                <p className='imagenCarruselText'>PARACAIDISMO</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/MOTOS DE AGUA'>

            <div title='MOTOS DE AGUA'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${motosdeaguaImg})` }}>
                <p className='imagenCarruselText'>MOTOS DE AGUA</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/SUBMARINISMO'>

            <div title='SUBMARINISMO' >
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${submarinismoImg})` }}>
                <p className='imagenCarruselText'>SUBMARINISMO</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/DESCENSOS MTB'>

            <div title='DESCENSOS MTB'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${descensoMTBImg})` }}>
                <p className='imagenCarruselText'>DESCENSOS MTB</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/RUTAS EN TODOTERRENO'>

            <div title='RUTAS EN TODOTERRENO'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${rutaTodoterrenoImg})` }}>
                <p className='imagenCarruselText'>TODOTERRENO</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/SURFING'>

            <div title='SURFING' >
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${surfImg})` }}>
                <p className='imagenCarruselText'>SURFING</p>
              </div>
            </div>
            </Link>
        </nav>

        <nav>
        <Link to='./deporte/WINDSURF'>

            <div title='WINDSURF'>
              <div className='imagenCarrusel'style={{ backgroundImage:  `url(${windsurfImg})` }}>
                <p className='imagenCarruselText'>WINDSURF</p>
              </div>
            </div>
            </Link>
        </nav>


      </Slider>
    </div>
  );


}
export default Carrusel