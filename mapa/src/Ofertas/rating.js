import React, { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom'

import { useSelector } from 'react-redux'
import './rating.css'


const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const CrearRating = () => {
  const { id } = useParams()
  const [rate, setRate] = useFormField()


  const user = useSelector(s => s.user)
  const historial = useHistory()
  const [isError, setError] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault()
    const oferta = { rate }
    setError(false)

    try {
      const red = await fetch(`http://localhost:8080/ofers/` + id, {
        method: 'POST',
        body: JSON.stringify(oferta),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user.token}`
        }
      })
      if (red.state === 400) {
        red.send('Esta oferta existe')

      }
      else {
        console.log('CrearRating')

        const datas = await red.json()
        if (datas.success) {
          console.log(rate)
         historial.push(`/`)
        } else {
          setError(true)
        }
      }
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }
  return (

    <div >
      <div >

        <form onSubmit={handleSubmit}>

        <input id='borde'  name='rate' required value={rate} onChange={setRate}
            
          />
          <input type="submit" class="fadeIn elfth" value="Puntua" />

        </form>
      </div>
    </div>
  )

}
export default CrearRating