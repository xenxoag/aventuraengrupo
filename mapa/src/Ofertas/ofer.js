import React, { useState, useEffect } from 'react'
import { useParams, Link } from 'react-router-dom'
import moment from 'moment'
import './ofer.css'
import '../App.css'
import CrearRating from './rating'
import Rateador from './Rateador'

const Ofer = () => {
  const { id } = useParams()
  const [ofer, setOfer] = useState()
 

  useEffect(() => {
    fetch('http://localhost:8080/ofers/' + id)
      .then(r => r.json())
      .then(data => setOfer(data))
  }, [id])
  
  if (!ofer) return 'oferta no encontrada'

  
  return (
    <div className="cartis">
      <section className='ofer1'>
      <h2 className="oferName">{ofer.nombre_oferta}</h2>
        <div id="restimage" className="col threeQuarter border">
          <div>
            
            <img src={ofer.imagen} draggable="false" className="itemImg" alt="Item " />
          </div>

          <div className="section gallery">
            <img src={ofer.imagen1} draggable="false" alt="imagen" />
            <img src={ofer.imagen2} draggable="false" alt="imagen" />
            <img src={ofer.imagen3} draggable="false" alt="imagen" />
          </div>
        </div>
        </section>
        <section className='ofer2'>

        <div className="col quarter padding">
          <div className="restoinfo">
          <p className='bofer'>Deporte</p>
          <p className="tags">{ofer.deporte}</p>
            <p className='bofer' >Lugar</p>
            <p className="place">{ofer.lugar}</p>
            <p className='bofer'>Provincia</p>
            <p className="descrip">{ofer.provincia}</p> 
            <p className='bofer'>Descripción</p>
            <p className="descrip">{ofer.decripcion}</p>            
            <p className='bofer'>Fecha</p>
            <p className="fech">{ofer && moment(ofer.fecha).format('DD/MM/YYYY')}</p>
            <p className='bofer'>Maximo personas</p>
            <p className="place">{ofer.maxpersonas}</p>
            <p className='bofer'>Precio</p>
          <div className="section price">
            <p>{ofer.precio}</p>
          </div>
        </div>

          <div className="section last">
          <Link to="/CompraRealizada"><h2 className="button" type="submit">RESERVA TU PLAZA</h2></Link>
          </div>
          
          <div id='trat'>
            <Rateador/>
          </div>
          <div id='crea'>
            <CrearRating />
          </div>
        </div>
        </section>
    </div>
  )
}



export default Ofer

