import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import './ofer.css'



const Rateador = () => {
    const { id } = useParams()
    const [rater, setRater] = useState()

    useEffect(() => {
        fetch('http://localhost:8080/rate/' + id)
            .then(r => r.json())
            .then(data => setRater(data))
    }, [id]
    )

    let finalRating = 0
    

    if (!rater) return 'oferta sin puntuar'

function calculameesta (rater) {
    for (let i = 0; i<rater.length; i++) {
        finalRating += parseInt(rater[i].rate)
    } finalRating = finalRating/rater.length.toFixed(1)
}
calculameesta(rater)


    return (


        <p>
            {finalRating.toFixed(1)}/5
        </p>
    )
}



export default Rateador

