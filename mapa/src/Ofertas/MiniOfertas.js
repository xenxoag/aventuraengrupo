import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import './MiniOfertas.css'
import Opciones from '../Opciones/Opciones'
import Rateador2 from './Rateador2'


const MiniOfertas = () => {
    const [oferta, setOferta] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/ofers')
            .then(r => r.json())
            .then(data => setOferta(data))
    }, [])

    if (!oferta) return 'Loading...'
    
    return (
        <section>
            <div className='titulo'>DESCUBRE LA AVENTURA DE TU VIDA</div>
            <Opciones /> 
        <div id="miniofertas"  >
            {oferta.map(oferta =>
                <Link to={`/ofers/${oferta.id}`} key={oferta.id}>
                    <div id="carta-oferta" >
                        <header >
                            <h1 className='cardheader'>{oferta.nombre_oferta}</h1>
                        </header>
                           
                        <img className=' imagen'src={oferta.imagen} alt=''/>
                        <div className='divis'> 
                        <section className='divisa'>                           
                                    <h2 id='cardh2'>{oferta.deporte}</h2>                               
                                    <h3>{oferta.lugar}</h3>                              
                                    <h4>{oferta.provincia}</h4>                                                                                      
                            </section>
                            <section >
                                <p>VALORACIÓN</p>
                                 <div className='divisb'><Rateador2 id={oferta.id} /></div>
                                 </section>  
                        </div>
                        <div className='precios'>
                       
                            <p >{oferta.precio}</p>
                        
                        </div>
                    </div>
                </Link>
            )}
        </div>
        </section>
    )
}




export default MiniOfertas
