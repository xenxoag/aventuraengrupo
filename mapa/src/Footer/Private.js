import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import './Private.css'






const Private = () => {
    const [oferta, setOferta] = useState(false)
    const Handleofer = () => setOferta(!oferta)
    const logout = () => dispatch({ type: 'logoutProov' })
    const dispatch = useDispatch()
    const proveedor = useSelector(s => s.proveedor)

    return (
        <div class="aparicion fadeInDown">
            <div id="recuadro">
                <div>
                    <p className="principio">BIENVENIDO:  {proveedor.username}</p>
                    
                </div>
                <Link to="/CrearOferta">
                    <div class="btn btn-1" onClick={Handleofer}>
                        <b>CREAR OFERTA</b>
                    </div>
                </Link>
                <Link to={`/promotor/${proveedor.username}`}>
                    <div className="btn btn-2" onClick={Handleofer}>
                        <b>BORRA O MODIFICA TU OFERTA</b>
                    </div>
                </Link>
                <div class="btn btn-4" onClick={logout}>
                    <b>CERRAR SESIÓN</b>
                </div>
                {/* <div className="cont">
            <p>BIENVENIDO</p>
            <h1>{proveedor.username}</h1>
            <br/>
            
            <br/>
            
            <br/>
            
        


        </div> */}


            </div>
        </div>
    )

}
export default Private