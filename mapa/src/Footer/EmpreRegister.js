import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'
import './EmpreRegister.css'
import { Link } from 'react-router-dom'

const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const EmpreRegister = () => {
  const [nombre_prov, setNombre_prov] = useFormField()
  const [nif, setNif] = useFormField()
  const [direccion, setDireccion] = useFormField()
  const [email, setEmail] = useFormField()
  const [nombre_cont, setNombre_cont] = useFormField()
  const [telefono, setTelefono] = useFormField()
  const [password, setPassword] = useFormField()
  const [secretKey, setSecretKey] = useFormField()


  const historiti = useHistory()
  const [mensaje, setMensaje] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    const user = { nombre_prov, nif, direccion, email, nombre_cont, telefono, password, secretKey} 
    setMensaje(false)
    try {
      const ret = await fetch('http://localhost:8080/proveedor/create', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json'
         
        }
      })
      const data = await ret.json()
     
      historiti.push(`/promotor/`)
      if (data.success) {
      } else {
        setMensaje(true)
      }
    } catch (err) {
      console.warn('Error:', err)

      setMensaje(true)
    }
  }
  return (
    <div class="wrapper">
      <div id="cajita" class="fadeInDown">
        <p className="concinfo">Promotror</p>
        <h1 id="entrar" class="inactive underlineHover"><Link to="/promotor">entrar</Link></h1>
        <h2 id="registrarse"class="active">Registrarse</h2>

        <form onSubmit={handleSubmit}>

          <input name='nombre_prov' required value={nombre_prov} onChange={setNombre_prov}
            type="text" id="nombreempresa" class="fadeIn first" placeholder="Nombre empresa" />


          <input name='nif' required value={nif} onChange={setNif}
            type="text" id="nif" class="fadeIn second" placeholder="Nif"
          />

          <input name='direccion' required value={direccion} onChange={setDireccion}
            type="text" id="direccion" class="fadeIn third" placeholder="Direccion"
          />

          <input name='email' required value={email} onChange={setEmail}
            type="text" id="email" class="fadeIn fourth" placeholder="Email"
          />

          <input name='nombre_cont' required value={nombre_cont} onChange={setNombre_cont}
            type="text" id="nombre_cont" class="fadeIn fifth" placeholder="Nombre de contacto"
          />

          <input name='telefono' required value={telefono} onChange={setTelefono}
            type="text" id="telefono" class="fadeIn sixth" placeholder="Telefono"
          />
          <input name='password' required value={password} onChange={setPassword}
            type="text" id="password" class="fadeIn seventh" placeholder="Password"
          />
          <p className="addinfo">Si no sabes lo que es el siguiente campo contacta con nsotros primero en el siguiente correo: aventurate@gmail.com</p>
          <input name='secretKey' required value={secretKey} onChange={setSecretKey}
            type="text" id="secretKey" class="fadeIn eigth" placeholder="secretKey"
          />
          <input type="submit" class="fadeIn nineth" value="entra" />
          {/* {!mensaje && <div>BIENVENIDO</div>} */}
          {mensaje && <div>Error, nombre registrado</div>}
        </form>
      </div>
    </div>
  )

}
export default EmpreRegister