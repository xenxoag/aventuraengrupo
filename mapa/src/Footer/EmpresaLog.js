import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import './EmpresaLog.css'

const jwt = require('jsonwebtoken')




const EmpresaLog = () => {
  const dispatch = useDispatch()
  const loginproov = (proveedor) => {
    dispatch({ type: 'loginProov', proveedor })
    dispatch({ type: 'logout'})
  }
  const [nombre_prov, setNombre_prov] = useState('')
  const [password, setPassword] = useState('')


  const [mensaje, setMensaje] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault()
    const proveedor = { nombre_prov, password }


    try {
      const ret = await fetch('http://localhost:8080/proveedor/login', {
        method: 'POST',
        body: JSON.stringify(proveedor),
        headers: {
          'Content-Type': 'application/json',
        }
      })

      const data = await ret.json()
      if (data.error === true) {
        setMensaje(true)
        return
      }
      if (!data.success === true || !('token' in data)) {
        throw new Error("error inesperado: Bug en la aplicación?")
      }
      let decoded = jwt.decode(data.token)
      decoded.token = data.token
    

      localStorage.setItem("token", data.token)
      localStorage.setItem("proveedor", decoded.nombre_prov)
      localStorage.setItem("id", decoded.id)
    
      loginproov({
        username: decoded.nombre_prov,
        id:decoded.id,
        token: data.token

      })

      setMensaje(false)
      console.log(data)


    } catch (err) {
      console.log(err)
    }

  }

  return (
    <div className="wrap">
      
      <div id="logueador" className="fadeInDown">
      <p className="concinfo">Promotor</p>
      <h2 id="entrare"className="active"> entrar </h2>
      <h2 id="rejister"className="inactive underlineHover"><Link to="/EmpreRegister">Registrarse</Link></h2>
      <p className="addinfo">Introduce tus datos de acceso</p>
        <form onSubmit={handleSubmit}>
          {mensaje && <p>proveedor no encontrado</p>}
          
          <input 
              required
              value={nombre_prov}
              onChange={e => setNombre_prov(e.target.value)}
              type="text" id="login" className="fadeIn second"  placeholder="Username"
            />
          <input 
              name="password"
             
              required
              value={password}
              onChange={e => setPassword(e.target.value)}
              type="text" id="password" className="fadeIn third"  placeholder="Password"
            />
          <input id='imputs' type="submit" className="fadeIn fourth" value="entra" />
        </form>
      </div>
    </div>
  )
}

export default EmpresaLog
