import React, { useState, useEffect } from 'react'
import { useParams,  useHistory } from 'react-router-dom'
import {  useSelector  } from 'react-redux'
import '../App.css'



const EditOffer = () => {
  const { id } = useParams()
  const [ofer, setOfer] = useState()
  const historial = useHistory()
  const proveedor = useSelector(s => s.proveedor)
  const [isError, setError] = useState(false)

  const handleChange = e => {
    setOfer({ ...ofer, [e.target.id]: e.target.value })
  }

  useEffect(() => {
    fetch('http://localhost:8080/ofers/' + id)
      .then(r => r.json())
      .then(data => setOfer(data))
  }, [id])
  
  if (!ofer) return 'oferta no encontrada'
  
  const handleSubmit = async (e) => {
    e.preventDefault()
  
    

    setError(false)
    
    try {
      const red = await fetch('http://localhost:8080/ofers', {
        method: 'PUT',
        body: JSON.stringify(ofer),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${proveedor.token}`
        }
      })
      if (red.state === 400) {
        red.send('Esta oferta existe')

      }
      else {
        const datas = await red.json()
       console.log(datas)
        if (datas.success) {
          historial.push(`/`)
        } else {
          setError(true)
        }
      }
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }

}
  return (
    <div className="wrapper ">
      <div id="parametros" className="fadeInDown">

      <h2 className="active">edita tu oferta</h2>
      <p>Por favor, cambia los campos que creas necesarios con la informacion de tu oferta </p>

      <form onSubmit={handleSubmit}>

        <input name='deporte' value={ofer && ofer.deporte} onChange={handleChange}
          type="text" id="deporte" className="fadeIn first" placeholder={ofer.deporte} 
        />
        <input name='nombre_oferta' required value={ofer && ofer.nombre_oferta} onChange={handleChange}
          type="text" id="nombre_oferta" className="fadeIn second" placeholder={handleChange} readOnly
        />
        <input name='lugar' required value={ofer && ofer.lugar} onChange={handleChange}
          type="text" id="lugar" className="fadeIn third" placeholder={ofer.lugar}
        />
        <input name='decripcion' required value={ofer && ofer.decripcion} onChange={handleChange}
          type="text" id="decripcion" className="fadeIn fourth" placeholder={ofer.decripcion}
        />
        <input name='precio' type='text' required value={ofer && ofer.precio} onChange={handleChange}
          id="precio" className="fadeIn fifth" placeholder={ofer.precio}
        />
        <input name='provincia' required value={ofer && ofer.provincia} onChange={handleChange}
          type="text" id="provincia" className="fadeIn sixth" placeholder={ofer.provincia}
        />
         <input name='maxpersonas' required value={ofer && ofer.maxpersonas} onChange={handleChange}
          type="text" id="maxpersonas" className="fadeIn sixth" placeholder={ofer.maxpersonas}
        />
        <input name='fecha' type='date' required value={ofer.fecha} onChange={handleChange}
          id="fecha" className="fadeIn seventh" placeholder={ofer.fecha}
        />
        <input name='imagen' required value={ofer && ofer.imagen} onChange={handleChange}
          type="text" id="imagen" className="fadeIn eigth" placeholder={ofer.imagen}
        />
        <input name='imagen1' required value={ofer && ofer.imagen1} onChange={handleChange}
          type="text" id="imagen1" className="fadeIn nineth" placeholder={ofer.imagen1}
        />
        <input name='imagen2' required value={ofer && ofer.imagen2} onChange={handleChange}
          type="text" id="imagen2" className="fadeIn tenth" placeholder={ofer.imagen2}
        />
        <input name='imagen3' required value={ofer && ofer.imagen3} onChange={handleChange}
          type="text" id="imagen3" className="fadeIn tenth" placeholder={ofer.imagen3}
        />
        <input type="submit" class="fadeIn elfth" value="Editar Oferta" />
        {isError && <div>¡¡BIENVENIDO</div>}

      </form>
    </div>
  </div>
)

}
    
    
    

export default EditOffer

