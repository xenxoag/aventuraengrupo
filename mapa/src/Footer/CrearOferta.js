import React, { useState } from 'react';
import { useHistory } from 'react-router-dom'

import { useSelector } from 'react-redux'
import './CrearOferta.css'


const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const CrearOferta = () => {
  const [deporte, setDeporte] = useFormField()
  const [nombre_oferta, setNombre_oferta] = useFormField()
  const [lugar, setLugar] = useFormField()
  const [decripcion, setDecripcion] = useFormField()
  const [precio, setPrecio] = useFormField()
  const [provincia, setProvincia] = useFormField()
  const [maxpersonas, setMaxpersonas] = useFormField()
  const [fecha, setFecha] = useFormField()
  const [imagen, setImagen] = useFormField()
  const [imagen1, setImagen1] = useFormField()
  const [imagen2, setImagen2] = useFormField()
  const [imagen3, setImagen3] = useFormField()
  const proveedor = useSelector(s => s.proveedor)
  const historial = useHistory()
  const [isError, setError] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault()
    const oferta = { deporte, nombre_oferta, lugar, decripcion, precio, provincia, maxpersonas, fecha, imagen, imagen1, imagen2, imagen3}
    setError(false)

    try {
      const red = await fetch('http://localhost:8080/ofers', {
        method: 'POST',
        body: JSON.stringify(oferta),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${proveedor.token}`
        }
      })
      if (red.state === 400) {
        red.send('Esta oferta existe')

      }
      else {
        const datas = await red.json()
       console.log(datas)
        if (datas.success) {
          historial.push(`/ofers/${datas.id}`)
        } else {
          setError(true)
        }
      }
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }
  return (
    <div className="wrapper ">
      <div id="parametros" className="fadeInDown">

        <h2 className="active">Crea tu oferta</h2>
        <p className="info" >Por favor, rellena los campos con la informacion de tu oferta de viaje</p>

        <form onSubmit={handleSubmit}>

          <input name='deporte' required value={deporte} onChange={setDeporte}
            type="text" id="deporte" className="fadeIn first" placeholder="DEPORTE"
          />
          <input name='nombre_oferta' required value={nombre_oferta} onChange={setNombre_oferta}
            type="text" id="nombre_oferta" className="fadeIn second" placeholder="NOMBRE OFERTA"
          />
          <input name='lugar' required value={lugar} onChange={setLugar}
            type="text" id="lugar" className="fadeIn third" placeholder="LUGAR"
          />
          <input name='decripcion' required value={decripcion} onChange={setDecripcion}
            type="text" id="decripcion" className="fadeIn fourth" placeholder="DESCRIPCIÓN"
          />
          <input name='precio' type='text' required value={precio} onChange={setPrecio}
            id="precio" className="fadeIn fifth" placeholder="PRECIO"
          />
          <input name='provincia' required value={provincia} onChange={setProvincia}
            type="text" id="provincia" className="fadeIn sixth" placeholder="PROVINCIA"
          />
           <input name=' maxpersonas'  type='text' required value={maxpersonas} onChange={setMaxpersonas}
            id="provincia" className="fadeIn seventh" placeholder="maxpersonas"
          />
          <input name='fecha' type='date' required value={fecha} onChange={setFecha}
            id="fecha" className="fadeIn seventh" placeholder="dd/mm/yyyy"
          />
          <input name='imagen' required value={imagen} onChange={setImagen}
            type="text" id="imagen" className="fadeIn eigth" placeholder="IMAGEN (url)"
          />
          <input name='imagen1' required value={imagen1} onChange={setImagen1}
            type="text" id="imagen1" className="fadeIn nineth" placeholder="IMAGEN 1 (url)"
          />
          <input name='imagen2' required value={imagen2} onChange={setImagen2}
            type="text" id="imagen1" className="fadeIn tenth" placeholder="IMAGEN 2 (url)"
          />
           <input name='imagen3' required value={imagen3} onChange={setImagen3}
            type="text" id="imagen1" className="fadeIn tenth" placeholder="IMAGEN 3 (url)"
          />
          <input type="submit" class="fadeIn elfth" value="Publicar Oferta" />
          {isError && <div>¡¡BIENVENIDO</div>}

        </form>
      </div>
    </div>
  )

}
export default CrearOferta