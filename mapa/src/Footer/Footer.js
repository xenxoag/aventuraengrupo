import React from 'react';
import {Link} from'react-router-dom';
import  './Footer.css'

const Footer= () =>{
    return(
        <footer className="footer">
        <section className="flex-rw">
            <span className="overlap footer-social-connect">
                CONNECT <span className="footer-social-small">with</span> US:</span>
            <span className="lapover footer-social-icons-wrapper">
               <u><Link to="/nosotros" >Personal info</Link></u>
            </span>
        </section>
    </footer>
    )
}
export default Footer