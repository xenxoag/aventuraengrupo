import EmpresaLog from "./EmpresaLog"
import React from 'react'
import { useSelector } from 'react-redux'
import Private from './Private'

const EmpreEnter = () => {

    const proveedor = useSelector(s => s.proveedor)
    return (
      <div className="App">
        {!proveedor && <EmpresaLog />}
        {proveedor && <Private />}
      </div>
    )
  }
  export default EmpreEnter