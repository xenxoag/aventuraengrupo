import React, { useState, useEffect } from 'react'
import { useParams,  useHistory } from 'react-router-dom'
import {  useSelector  } from 'react-redux'
import '../Ofertas/ofer.css'



const DeleteOffer = () => {
  const { id } = useParams()
  const [ofer, setOfer] = useState()
  const historial = useHistory()
  const proveedor = useSelector(s => s.proveedor)
  const [isError, setError] = useState(false)
  useEffect(() => {
    fetch('http://localhost:8080/ofers/' + id)
      .then(r => r.json())
      .then(data => setOfer(data))
  }, [id])
  
  if (!ofer) return 'oferta no encontrada'
  
  const handleSubmit = async (e) => {
    e.preventDefault()
    

    if (!window.confirm("Seguro que queres eliminar??")) { 
       return;
   }

    setError(false)

    try {
      const red = await fetch('http://localhost:8080/ofers', {
        method: 'DELETE',
        body: JSON.stringify(ofer),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${proveedor.token}`
        }
      })
      if (red.state === 400) {
        red.send('Esta oferta existe')

      }
      else {
        const datas = await red.json()
       console.log(datas)
        if (datas.success) {
          historial.push(`/`)
        } else {
          setError(true)
        }
      }
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }

}
  return (
      
    <div>
        
      <div className="contido">
        <div className="col threeQuarter border">
          <div className="">
            <h2 className="oferName">{ofer.nombre_oferta}</h2>
            <img src={ofer.imagen} draggable="false" className="itemImg" alt="Item " />
          </div>

          <div className="section gallery">
            <img src={ofer.imagen1} draggable="false" alt="imagen" />
            <img src={ofer.imagen2} draggable="false" alt="imagen" />
          </div>
        </div>

        <div className="col quarter padding">
          
            <p  class="descrip">{ofer.decripcion}</p>
            <p  class="fech">{ofer.fecha}</p>
            <p class="place">{ofer.lugar}</p>
            <p class="tags">{ofer.deporte}</p>
       

          <div className="section price">
            <p>{ofer.precio}</p>
          </div>

          <div className="section last">
          <button className='buttonEdit'onClick={handleSubmit}>ELIMINAR OFERTA</button>
          </div>
        </div>
      </div>

    </div>
    
  )
}



export default DeleteOffer

