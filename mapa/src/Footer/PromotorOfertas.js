import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom'
import '../Ofertas/MiniOfertas.css'


const PromotorOfertas = () => {

    const { name } = useParams()
    const [oferta, setOferta] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/ofers')
            .then(r => r.json())
            .then(data => setOferta(data))
    }, [])

    if (!oferta) return 'loading...'
    const entries = oferta.filter(i => i.user_id === name)
    if (!entries.length) return 'no tienes ninguna oferta creada'


    return (
        <div id="miniofertas">
            {entries.map(oferta =>

                <div id="carta-oferta" key={oferta.deporte}>
                    <header>
                        <h1 className='cardheader'>{oferta.nombre_oferta}</h1>
                    </header>
                    <section >
                        <img className=' imagen' src={oferta.imagen} alt='' />
                        <ul>
                            <li>
                                <h2 id='cardh2'>{oferta.deporte}</h2>
                            </li>
                            <span>
                                <Link to={`/promotor/modificar/${oferta.id}`}>
                                    <button className='buttonEdit'>editar oferta</button></Link>
                                <Link to={`/promotor/delete/${oferta.id}`}>
                                    <button className='buttonEdit'>borrar oferta</button></Link>
                            </span>
                        </ul>
                    </section>
                    <div className='precios'>
                        <p >{oferta.precio}</p>
                    </div>
                </div>

            )}
        </div>
    )
}



export default PromotorOfertas