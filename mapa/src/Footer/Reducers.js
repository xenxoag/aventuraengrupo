import { combineReducers } from 'redux'

const userReducer = (state, action) => {
    switch (action.type) {
        case 'login': return action.user
        case 'logout': return null
        default: return state || null
    }
}

const proveedorReducer = (state, action)=>{
    switch (action.type) {
        case "loginProov": return action.proveedor;
        case "logoutProov": return null;
        default: return state || null
    }
}

const modalReducer = (state, action) => {
    switch (action.type) {
        case 'showModal': return { type: action.modalType }
        case 'hideModal': return null
        default: return state || null
    }
}

const productReducer = (state, action) =>
    action.type === 'products' ? action.products : state || null

const rootReducer = combineReducers({
    user: userReducer,
    modal: modalReducer,
    products: productReducer,
    proveedor: proveedorReducer,
})

export default rootReducer