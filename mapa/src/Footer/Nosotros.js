import React, { Component } from 'react';

import CV from 'react-cv'

class Nosotros extends Component {
    render() {
        return (
            <div>
                <div>
                    <CV
                        personalData={{
                            name: 'Xenxo Álvarez',
                            title: 'full.stack developer',
                            image: 'https://media-exp1.licdn.com/dms/image/C5603AQHuI_I21xwArA/profile-displayphoto-shrink_200_200/0?e=1586995200&v=beta&t=tJ4NkSeZqFWsmWODH0y6KxHJVc--KTtVITml8-Cf16A',
                            contacts: [
                                { type: 'email', value: 'xenxoag@gmail.com' },
                                { type: 'phone', value: '678107107' },
                                { type: 'location', value: 'Vigo' },
                                { type: 'linkedin', value: 'https://www.linkedin.com/in/xenxo-%C3%A1lvarez-garc%C3%ADa-93373a193/' },
                                { type: 'twitter', value: 'twitter.com/404' },
                                { type: 'github', value: 'https://gitlab.com/dashboard/projects' }
                            ]
                        }}
                        sections={[
                            {
                                type: 'text',
                                title: 'Career Profile',
                                content: 'La programación es un mmundo interesante y apasionante, donde uno puede dar rienda suelta a su imaginación. Quien no desea a veces plasmar sus ideas de alguna forma y no tiene formas de hacerlo. La progranación es la solución. Quizas al no tener una idea clara de hacia donde llevar mi vida, busque salida en la programación por el interes depertado en Bachillerato. y así decidí seguir investigando y descubriendo más sobre este, aún insólito e inexplorado, mundo ',
                                icon: 'usertie'
                            },
                            {
                                type: 'common-list',
                                title: 'Education',
                                icon: 'graduation',
                                items: [
                                    {
                                        title: 'Hack a boss',
                                        authority: 'Private Center',
                                        authorityWebSite: 'https://hack-a-bos.com/',
                                        rightSide: '2019-2020'
                                    },
                                    {
                                        title: 'Energías Renovables',
                                        authority: 'Cifp Paz andrade',
                                        authorityWebSite: 'http://www.cifpvalentinpazandrade.es/es/',
                                        rightSide: '2019 - Present'
                                    },
                                    {
                                        title: 'Bachillerato',
                                        authority: 'IES Ricardo Mella',
                                        authorityWebSite: 'https://www.edu.xunta.gal/centros/iesricardomella/',
                                        rightSide: '2017-2019'
                                    }
                                ]
                            },
                            {
                                type: 'experiences-list',
                                title: 'Experiences',
                                description: '',
                                icon: 'archive',
                                items: [
                                    {
                                        title: 'Lead Software Developer',
                                        company: 'HaB Company', /* link */
                                        description: 'I\'m learn to be a great developer yeeeey!',
                                        companyWebSite: /*'Link a compañia de referencia'*/'',
                                        companyMeta: 'Spain',
                                        datesBetween: '2017.10 - Present',
                                        descriptionTags: ['HTML', 'Javascript', 'CSS', 'SQL', 'React']
                                    },
                                ]
                            },
                            {
                                type: 'projects-list',
                                title: 'Projects',
                                description: 'Aventura en grupo has sido mi primer proyecto real y el unico hasta el momento',
                                icon: 'tasks',
                                groups: [
                                    {
                                        sectionHeader: 'Hack a Boss',
                                        // description: '',
                                        items: [
                                            // { title: 'Project', projectUrl: 'optional', description: 'Optional' },
                                            // { title: 'Project', projectUrl: 'optional', description: 'Optional' },
                                            { title: 'AEG', projectUrl: 'http://aventuraengrupo.info/', description: 'hab proyect' }
                                        ]
                                    }
                                ]
                            },
                            {
                                type: 'common-list',
                                title: 'Languages',
                                icon: 'language',
                                items: [
                                    {
                                        authority: 'Spanish',
                                        authorityMeta: 'Native'
                                    },
                                    {
                                        authority: 'Galego',
                                        authorityMeta: 'Native'
                                    },
                                    {
                                        authority: 'English',
                                        authorityMeta: 'Lv A2 in the languaje school and Lv of Bachiller '
                                    },
                                    {
                                        authority: 'Deutch',
                                        authorityMeta: 'Lv A1 in the languaje school, I started A2 but I didnt finish it'
                                    }
                                ]
                            },
                            {
                                type: 'tag-list',
                                title: 'Hobies & Interests',
                                icon: 'cubes',
                                items: ['Gym', 'Rubik cubes']
                            }
                        ]}
                        branding={false}
                    />
                </div>
                <div>
                    <CV
                        personalData={{
                            name: 'Fernando Rúa',
                            title: 'full.stack developer',
                            image: 'https://media-exp1.licdn.com/dms/image/C5603AQHRI1Aw4yjqhg/profile-displayphoto-shrink_200_200/0?e=1586995200&v=beta&t=mMh6TVImfcz55aZnSjEjROSrVAPQZmqGizCOkRm8fm8',
                            contacts: [
                                { type: 'email', value: 'fer-rua@hotmail.es' },
                                { type: 'phone', value: '678409220' },
                                { type: 'location', value: 'Moaña' },
                                { type: 'website', value: 'example.com' },
                                { type: 'linkedin', value: 'https://www.linkedin.com/in/fernando-rua-cancelas-a3ab78195/' },
                                { type: 'twitter', value: '#ferrucan' },
                                { type: 'github', value: 'https://gitlab.com/ferrucan' }
                            ]
                        }}
                        sections={[
                            {
                                type: 'text',
                                title: 'Autonomo',
                                content: 'Un curioso con muchas ganas de aprender.', 
                                icon: 'usertie'
                            },
                            {
                                type: 'common-list',
                                title: 'Education',
                                icon: 'graduation',
                                items: [
                                    {
                                        title: 'Hack a boss',
                                        authority: 'private center',
                                        authorityWebSite: 'https://hack-a-bos.com/',
                                        rightSide: '2019-2020'
                                    },
                                    {
                                        title: 'Delineación',
                                        authority: 'Cifp Paz andrade',
                                        authorityWebSite: 'https://sample.edu',
                                        rightSide: '2019 - Present'
                                    }
                                ]
                            },
                            {
                                type: 'experiences-list',
                                title: 'Experiences',
                                description: '',
                                icon: 'archive',
                                items: [
                                    {
                                        title: 'Lead Software Developer',
                                        company: 'HaB Company', /* link */
                                        description: 'I\'m learn to be a great developer yeeeey!',
                                        companyWebSite: /*'Link a compañia de referencia'*/'',
                                        companyMeta: 'Spain',
                                        datesBetween: '2017.10 - Present',
                                        descriptionTags: ['HTML', 'Javascript', 'CSS', 'SQL', 'React']
                                    },
                                ]
                            },
                            {
                                type: 'common-list',
                                title: 'Conferences & Certificates',
                                description: '',
                                icon: 'comments',
                                items: [
                                    // {
                                    //     title: 'Some Conferences / 2019',
                                    //     authority: 'SomeConf',
                                    //     authorityMeta: 'Speaker',
                                    //     authorityWebSite: 'https://www.someconf.somesome',
                                    //     rightSide: 'test'
                                    // },
                                ]
                            },
                            {
                                type: 'projects-list',
                                title: 'Projects',
                                description: 'Aventura en grupo has sido mi primer proyecto real y el unico hasta el momento',
                                icon: 'tasks',
                                groups: [
                                    {
                                        sectionHeader: 'Hack a Boss',
                                        // description: '',
                                        items: [
                                            // { title: 'Project', projectUrl: 'optional', description: 'Optional' },
                                            // { title: 'Project', projectUrl: 'optional', description: 'Optional' },
                                            { title: 'AEG', projectUrl: 'http://aventuraengrupo.info/', description: 'hab proyect' }
                                        ]
                                    }
                                ]
                            },
                            {
                                type: 'common-list',
                                title: 'Languages',
                                icon: 'language',
                                items: [
                                    {
                                        authority: 'Spanish',
                                        authorityMeta: 'Native'
                                    },
                                    {
                                        authority: 'Galego',
                                        authorityMeta: 'Native'
                                    },
                                    
                                ]
                            },
                            {
                                type: 'tag-list',
                                title: 'Hobies & Interests',
                                icon: 'cubes',
                                items: ['Lectura', 'Remo']
                            }
                        ]}
                        branding={false}
                    />
                </div>
            </div>

        )
    }
} export default Nosotros