import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import './opciones.css'


const Opciones = () => {
    const [fecha, setFecha] = useState()
    const [precio, setPrecio] = useState()
    const history = useHistory()

    const handleFechaSubmit = (e) => {
        e.preventDefault()
        history.push(`/fecha/${fecha}`)
    }
    const handlePrecioSubmit = (e) => {
        e.preventDefault()
        history.push(`/precio/${precio}`)
    }
    const [deporte, setDeporte] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/deporte')
            .then(r => r.json())
            .then(data => setDeporte(data))
    }, [])


    if (!deporte) return 'Loading...'
    return (
        <nav className='nav'>
            <div className='ul'>
                <li>
                    <p >DEPORTES</p>
                    <ul >
                        {deporte.map(deporte =>
                            <Link to={`/deporte/${deporte}`} key={deporte}><li  ><p >{deporte}</p></li></Link>)}

                    </ul>
                </li>
                <li>
                    <p >PROVINCIA</p>
                    <ul>
                        <Link to={`/provincia/Coruña`}><li><p >Coruña</p></li></Link>
                        <Link to={`/provincia/Lugo`}> <li><p >Lugo</p></li></Link>
                        <Link to={`/provincia/Ourense`}><li><p >Ourense</p></li></Link>
                        <Link to={`/provincia/Pontevedra`}><li><p >Pontevedra</p></li></Link>
                    </ul>
                </li>
                <li>
                    <p >PRECIO</p>
                    <ul className='ulopciones'>
                        <form id='precio' onSubmit={handlePrecioSubmit} >
                            <label className='inputprecio'>
                                <input name='precio' 
                                    required value={precio}
                                    onChange={e => setPrecio(e.target.value)} />
                            </label>
                            <button className='buttonSearch'>BUSCAR</button>
                        </form>
                    </ul>
                </li>
                <li><p>FECHA</p>
                    <ul className='ulopciones' >
                        <form onSubmit={handleFechaSubmit}>
                            <label >
                                <input
                                    name='fecha'
                                    type='date'
                                    required value={fecha}
                                    onChange={e => setFecha(e.target.value)} />
                            </label>
                            <button className='buttonSearch'>BUSCAR</button>
                        </form>
                    </ul>
                </li>
            </div>
        </nav>



    )

}

export default Opciones