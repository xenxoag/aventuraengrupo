
const { Pool } = require('pg');
require('dotenv').config()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const jwtsecret = 'aventura'
const pool = new Pool({
  connectionString: process.env.DATABASE_URL
})





const getUsers = (request, response) => {
  pool.query('SELECT * FROM aeg_schema.usuario ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUser = async (request, response) => {
  const { nombre_usuario, apellidos, email, password } = request.body
  const BCRYPT_SALT_ROUNDS = 12;
  try {
    const client = await pool.connect();

    const userexist = await pool.query(`SELECT * FROM aeg_schema.usuario WHERE email= $1 or nombre_usuario= $2`, [email, nombre_usuario])

    if (userexist.rows.length !== 0) {
      response.status(400).json({ error: true })
      return
    }
    const hashpassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS)

    const user = await pool.query('INSERT INTO aeg_schema.usuario (nombre_usuario, apellidos, email, password) VALUES ($1, $2, $3, $4)',
      [nombre_usuario, apellidos, email, hashpassword]);

    client.release();
    response.json({ success: true })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}

const getBusiness = (request, response) => {
  pool.query('SELECT * FROM aeg_schema.provedores ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createBusiness = async (request, response) => {
  const { nombre_prov, nif, direccion, email, nombre_cont, telefono, password } = request.body

  try {
    const client = await pool.connect();

    const userexist = await pool.query(`SELECT * FROM aeg_schema.provedores WHERE email= $1`, [email])

    if (userexist.rows.length !== 0) {
      response.status(400).json({ error: true })
      return
    }

    const userBis = await pool.query('INSERT INTO aeg_schema.provedores (nombre_prov, nif, direccion, email, nombre_cont, telefono, password) VALUES ($1, $2, $3, $4, $5, $6)',
      [nombre_prov, nif, direccion, email, nombre_cont, telefono, password]);

    client.release();
    response.json({ success: true })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}


const Login = async (request, response) => {
  const { nombre_usuario, password } = request.body

  try {
    const client = await pool.connect();
    const userexist = await pool.query(`SELECT * FROM aeg_schema.usuario WHERE nombre_usuario= $1`, [nombre_usuario])
    if (userexist.rows.length === 0) {
      response.status(400).json({ error: true })
      console.log('cliente no encontrado')
      return
    }
    const comp = await bcrypt.compare(password, userexist.rows[0].password);
    if (!comp) {
      response.status(400).json({ error: true })
      console.log('cliente no encontrado')
      return
    }
    client.release();

    let tokendata = {nombre_usuario:nombre_usuario}
    let token =jwt.sign(tokendata,jwtsecret,{algorithm:'HS256',expiresIn:86400})
  
    response.json({ success: true, token:token})
  
  } catch (err) {
    console.warn('Error:', err)
    response.status(400).json({ error: true })
    return
  }  

};


const getOfertas = (request, response) => {
  pool.query('SELECT * FROM aeg_schema.ofertas ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createOfertas = async (request, response) => {
  const { deporte, nombre_oferta, lugar, decripcion, precio, provincia, fecha, maxpersonas, imagen, imagen1, imagen2 , imagen3} = request.body

  try {
    const ofer = await pool.connect();

    const oferexist = await pool.query(`SELECT * FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [nombre_oferta])

    if (oferexist.rows.length !== 0) {
      response.status(400).json({ error: true })
      return
    }
    
    const sell = await pool.query('INSERT INTO aeg_schema.ofertas (deporte, nombre_oferta, lugar, decripcion, precio, provincia, fecha, maxpersonas, imagen, imagen1, imagen2, imagen3, user_id ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)',
      [deporte, nombre_oferta, lugar, decripcion, precio, provincia, fecha, maxpersonas, imagen, imagen1, imagen2, imagen3, request.decodedToken.nombre_prov ]);
      const idOfer = await pool.query(`SELECT id FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [nombre_oferta])
      ofer.release(request.decodedToken);
      
    response.json({ success: true, id:idOfer.rows[0].id })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}

const getRateByIdOfer = async(request, response) => {
  const { rows } = await pool.query(
    'SELECT * FROM  aeg_schema.rating WHERE id_oferta = $1',
    [request.params.id_oferta]
  ) 
  if (rows.length) {
    response.json(rows)
  } else {
    response.status(404)
    response.json(null)
  }
}

const createRating = async (request, response) => {
  const { rate } = request.body

  try {
    const rated = await pool.connect();
    
    const sell = await pool.query('INSERT INTO aeg_schema.rating ( id_oferta, rate ) VALUES ($1, $2)',
      [ request.params.id_oferta, rate ]);
      rated.release();
      
    response.json({ success: true, sell})
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}


const DeleteOfer = async (request, response) => {
  const { nombre_oferta } = request.body
  try {
    const ofer = await pool.connect();

    const oferexist = await pool.query(`SELECT * FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [nombre_oferta])


    if (oferexist.rows.length === 0) {
      response.status(402).json({ error: true })
      return
    }
   
    const idOfer = await pool.query(`DELETE FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [oferexist.rows[0].nombre_oferta])
      ofer.release();
   
    response.json({ success: true,  })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}
const EditOfert = async (request, response) =>{
  const { deporte, nombre_oferta, lugar, decripcion, precio, provincia, fecha, maxpersonas, imagen, imagen1, imagen2, imagen3 } = request.body

  try {
    const ofer = await pool.connect();

    const oferexist = await pool.query(`SELECT * FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [nombre_oferta])
    if (oferexist.rows.length === 0) {
      response.status(403).json({ error: true })
      return
    }
    
    const sell = await pool.query(`UPDATE aeg_schema.ofertas SET deporte='${deporte}', nombre_oferta= '${nombre_oferta}', lugar='${lugar}', decripcion='${decripcion}', 
    precio='${precio}', provincia='${provincia}', fecha='${fecha}', maxpersonas='${maxpersonas}', imagen='${imagen}', imagen1='${imagen1}', imagen2='${imagen2}', imagen3='${imagen3}' WHERE nombre_oferta='${nombre_oferta}'` )
    
      const idOfer = await pool.query(`SELECT id FROM aeg_schema.ofertas WHERE nombre_oferta= $1`, [nombre_oferta])
      ofer.release(request.decodedToken);
      
    response.json({ success: true, id:idOfer.rows[0].id })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}



const getOferId = async(request, response) => {
  const { rows } = await pool.query(
    'SELECT * FROM  aeg_schema.ofertas WHERE id = $1',
    [request.params.id]

  )
  if (rows.length) {
    response.json(rows[0])
  } else {
    response.status(404)
    response.json(null)
  }
}


const getOferDep = async(request, response) => {
  const { rows } = await pool.query(
    'SELECT * FROM  aeg_schema.ofertas WHERE deporte = $1',
    [request.params.deporte]

  )
  if (rows.length) {
    response.json(rows[0])
  } else {
    response.status(404)
    response.json(null)
  }

}

const claveSecreta = '1234'
const createProveedor = async (request, response) => {
  const { nombre_prov, nif, email, direccion, nombre_cont, telefono, password, secretKey } = request.body
  const BCRYPT_SALT_ROUNDS = 12;
  if (secretKey !== claveSecreta){
    response.status(400).json({ error: true, permiso: 'denegado' })
    return
  }
 

  try {

    const userexist = await pool.query(`SELECT * FROM aeg_schema.provedores WHERE email= $1 or nombre_prov= $2`, [email, nombre_prov ])

    if (userexist.rows.length !== 0) {
      response.status(400).json({ error: true })
      return
    }
    const hashpassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS)
    const user = await pool.query('INSERT INTO aeg_schema.provedores (nombre_prov, nif, email, direccion, nombre_cont, telefono, password) VALUES ($1, $2, $3, $4, $5, $6, $7)',
      [nombre_prov, nif, email, direccion, nombre_cont, telefono, hashpassword]);

    response.json({ success: true })
  } catch (err) {
    console.warn('Error:', err)
      response.status(400).json({ error: true })
    return
  }
}
const LoginProveedor = async (request, response) => {
  const { nombre_prov, password } = request.body

  try {
    const client = await pool.connect();
    const userexist = await pool.query(`SELECT * FROM aeg_schema.provedores WHERE nombre_prov= $1`, [nombre_prov])
    
    if (userexist.rows.length === 0) {
      response.status(400).json({ error: true })
      console.log('proveedor no encontrado')
      return
    }
    const comp = await bcrypt.compare(password, userexist.rows[0].password);
    if (!comp) {
      response.status(400).json({ error: true })
      console.log('proveedor no encontrado')
      return
    }
  
    

    client.release();

    let tokendata = {nombre_prov:nombre_prov, id:userexist.rows[0].id}
    let token =jwt.sign(tokendata,jwtsecret,{algorithm:'HS256',expiresIn:86400})
  
    response.json({ success: true, token:token})
  
  } catch (err) {
    console.warn('Error:', err)
    response.status(400).json({ error: true })
    return

  }  

};

const getActivities = (request, response) => {
  pool.query('SELECT deporte FROM aeg_schema.ofertas  GROUP BY deporte', (error, results) => {
    if (error) {
      throw error
    }
    const   deporte = results.rows
    const deportes = []
    for(i in deporte){
      deportes.push(deporte[i].deporte)
    }   
    response.status(200).json(deportes)
  })
}



/*const getBusqueda = async (request, response) => {
  const parametros = request.query
  console.log(parametros)
 let q = 'SELECT * from ofertas'
  const condiciones = []
    if (parametros.deporte) { condiciones.push(`deporte = ${parametros.deporte}`)}
    if (parametros.provincia) { condiciones.push(`provincia = ${parametros.provincia}`)}
    if (parametros.precio) { condiciones.push(`precio = ${parametros.precio}`)}
    if (parametros.fecha) { condiciones.push(`fecha = ${parametros.fecha}`)}

    if (condiciones.length) { q = q + 'where ' +condiciones.join(' and ')}
  try {
    const client = await pool.connect();
    const busqueda = await pool.query(q);
      
    client.release();
    response.json(busqueda);
  } catch (err) {
    console.warn('Error:', err)
    //setMensaje(true)
  }*/


module.exports = {
  getUsers,
  createUser,
  getBusiness,
  createBusiness,
  Login,
  getOfertas,
  createOfertas,
  getRateByIdOfer,
  createRating,
  getOferId,
  getOferDep,
  LoginProveedor,
  createProveedor,
  getActivities,
  DeleteOfer,
  EditOfert,

}