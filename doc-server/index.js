
const express = require('express');
const bodyParser = require('body-parser');
const db = require('./controller')
const jwt = require('jsonwebtoken')
const cors =require('cors')


const jwtsecret = 'aventura'
const app = express();

app.use(cors({
  origin: '*',
  optionSuccessStatus: 200,

}))


app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());
app.set('trust proxy', true)


const AuthMiddleware = (req, res, next) => {
  try {
    const BEARER_END = 7;
    const authorizationHeader = req.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END).trim();
    let decodedToken = jwt.verify(requestToken, jwtsecret)
    req.decodedToken = decodedToken
    next()
  } catch (err) {
    console.log(err)
    res.status(403).send({error: true, msg: "No tienes permisos"})
  }
}


var port = process.env.PORT || 8080;

// Send message for default URL
app.get('/test', (req, res) => res.send('Express is running'));
app.post('/login', db.Login)
app.get('/users', db.getUsers)
app.post('/users', db.createUser)
app.get('/business', db.getBusiness)
app.post('/business', db.createBusiness)
app.get('/ofers', db.getOfertas)
app.post('/ofers' ,AuthMiddleware, db.createOfertas)
app.delete('/ofers' ,AuthMiddleware, db.DeleteOfer)
app.put('/ofers' ,AuthMiddleware, db.EditOfert)
app.get('/ofers/:id', db.getOferId)
app.get('/ofers/:proveedor', db.getOferId)
app.post('/proveedor/create', db.createProveedor)
app.post('/proveedor/login', db.LoginProveedor)
app.get('/deporte', db.getActivities)
app.post('/ofers/:id_oferta',db.createRating )
app.get('/rate/:id_oferta', db.getRateByIdOfer )

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})
